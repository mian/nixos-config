{ pkgs, device, swapsize, ... }:
{
  inherit (import ./disko-luks-btrfs.nix { inherit device swapsize; }) disko;

  my.sway-auto-lock.enable = true;

  services = {
    btrfs.autoScrub.enable = true;

    displayManager = {
      sddm = {
        enable = true;
        wayland.enable = true;
      };
      autoLogin = {
        enable = true;
        user = "ian";
      };
    };
  };
}
