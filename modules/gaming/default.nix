{ pkgs, config, lib, inputs, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc listOf path;
  cfg = config.my.gaming;
in
{
  options.my.gaming = {
    enable = mkOption {
      type = bool;
      default = false;
      description = mdDoc "Whether to enable my gaming environment.";
    };
  };

  config = mkIf cfg.enable (import ./gaming.nix { inherit pkgs config inputs; });
}
