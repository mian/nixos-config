{ pkgs, config, lib, inputs, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc listOf path;
  cfg = config.my.desktop;
in
{
  options.my.desktop = {
    enable = mkOption {
      type = bool;
      default = false;
      description = mdDoc "Whether to enable my desktop environment.";
    };
  };

  config = mkIf cfg.enable (mkMerge [
    (import ./desktop-system.nix { inherit pkgs config inputs; })
    { home-manager.users.ian = import ./desktop-home.nix { inherit pkgs config inputs; }; }
  ]);
}
