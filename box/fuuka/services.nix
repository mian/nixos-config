{ config, pkgs, inputs, ... }:
let
  # unstable = import ../../common/unstable.nix { inherit pkgs inputs; };
  nginx = import ./nginx.nix { inherit pkgs; };
in
{
  services = {
    inherit nginx;

    gotosocial = {
      enable = true;
      openFirewall = true;
      setupPostgresqlDB = true;
      settings = {
        application-name = "rainingmessages";
        host = "social.rainingmessages.dev";
        account-domain = "rainingmessages.dev";
        protocol = "https";
        bind-address = "0.0.0.0";
        port = 8080;
        trusted-proxies = [ "192.168.0.128" ];
        instance-inject-mastodon-version = true;
        instance-languages = [ "en" "ja" ];
        instance-federation-mode = "blocklist";
      };
    };

    minecraft-server = {
      enable = true;
      declarative = true;
      eula = true;
      openFirewall = true;
      serverProperties = {
        difficulty = "normal";
        level-name = "fuuka";
        level-seed = "fuuka";

        max-players = 4;
        motd = "Hello from NixOS";
        snooper-enabled = false;
      };
    };

    postgresql.package = pkgs.postgresql_16;

    postgresqlBackup = {
      enable = true;
      startAt = "*-*-* 05:00:00";
    };

    scrutiny = {
      enable = true;
      openFirewall = true;
      collector.enable = true;
      settings.web.listen.port = 8099;
    };

    thelounge = {
      enable = true;
      extraConfig = {
        reverseProxy = false;
        defaults = {
          name = "Libera.Chat";
          host = "irc.libera.chat";
          port = 6697;
          tls = true;
          rejectUnauthorized = true;
        };
      };
      plugins = with pkgs.theLoungePlugins.themes; [
        dracula
        solarized
        zenburn
      ];
    };

    wastebin = {
      enable = true;
      secretFile = "/etc/wastebin/env";
      settings = {
        WASTEBIN_BASE_URL = "https://bin.ian.tokyo";
        WASTEBIN_TITLE = "bin.ian.tokyo";
      };
    };
  };

  systemd.services = {
    pre-nginx = {
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
      wantedBy = [ "nginx.service" ];
      path = [ pkgs.coreutils ];
      script = ''
        mkdir -p /var/www
        chown nginx:nginx /var/www
      '';
    };

    pre-thelounge = {
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
      before = [ "thelounge.service" ];
      wantedBy = [ "thelounge.service" ];
      path = [ pkgs.coreutils ];
      script = ''
        until ${pkgs.bind.host}/bin/host irc.libera.chat; do sleep 10; done
      '';
    };
  };
}
