{ pkgs, config, inputs, modulesPath, ... }:
{
  imports = [ (modulesPath + "/profiles/qemu-guest.nix") ];

  my.audio.enable = true;
  my.desktop.enable = true;
  my.workstation-disk-encryption = {
    enable = true;
    device = "/dev/vda";
    swapsize = "1G";
  };

  boot = {
    kernelModules = [ "kvm-amd" ];
    initrd.availableKernelModules = [ "ahci" "xhci_pci" "virtio_pci" "sr_mod" "virtio_blk" ];
  };

  users.users.ian.initialPassword = "ian";

  networking.useDHCP = pkgs.lib.mkForce true;

  nixpkgs.hostPlatform = "x86_64-linux";

  system.stateVersion = "24.11";
}
