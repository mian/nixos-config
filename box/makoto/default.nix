{ config, pkgs, inputs, lib, modulesPath, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ./services.nix
    # ./containers.nix
  ];

  my.backup.home-to-local.enable = true;
  my.backup.home-to-ranni.enable = true;
  my.backup.srv-to-ranni = {
    enable = true;
    paths = [
      "/srv"
      "/var/backup"
      "/var/lib/hedgedoc"
    ];
  };
  my.home-network-only.enable = true;
  my.nebula-node.enable = true;

  networking.firewall.allowedTCPPorts = [ 3000 3456 6167 8099 8384 8989 ];

  powerManagement = {
    cpuFreqGovernor = "conservative";
    powertop.enable = true;
  };

  system.stateVersion = "24.05";
}
