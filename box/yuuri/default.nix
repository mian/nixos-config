{ pkgs, config, inputs, modulesPath, ... }:
let
  displayId = "eDP-1";
in
{
  imports = [
    inputs.nixos-hardware.nixosModules.gpd-pocket-4
    inputs.nixvirt.nixosModules.default
    ./hardware-configuration.nix
    ../nene/vms.nix
  ];

  my.audio.enable = true;
  my.backup.home-to-local.enable = true;
  my.desktop.enable = true;
  my.gaming.enable = true;
  my.nebula-node.enable = true;
  my.streaming.enable = true;
  my.workstation-disk-encryption = {
    enable = true;
    swapsize = "64G";
  };

  boot.kernelPackages = pkgs.lib.mkForce pkgs.linuxPackages_6_12;

  networking.networkmanager.enable = true;
  networking.extraHosts = pkgs.lib.mkForce ''
    10.10.10.117 ranni
    10.10.10.128 futaba
  '';

  programs.light.enable = true;

  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  home-manager.users.ian.wayland.windowManager.sway.config = {
    output.${displayId}.transform = "90";
    input."type:touch".map_to_output = displayId;
  };

  system.stateVersion = "24.11";
}
