{ pkgs, lib, inputs, config, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc listOf path;
  secretsConf = {
    sopsFile = ../../secrets/nebula-${config.networking.hostName}.yaml;
    owner = config.users.users.nebula-asgard.name;
    group = config.users.users.nebula-asgard.group;
  };
in
{
  options.my.nebula-node = {
    enable = mkOption {
      type = bool;
      default = false;
      description = mdDoc "Whether to run a nebula node.";
    };
  };

  config = mkIf config.my.nebula-node.enable {
    services.nebula.networks.asgard = {
      enable = true;
      ca = ./nebula-ca.crt;
      cert = "/run/secrets/nebula_crt";
      key = "/run/secrets/nebula_key";
      lighthouses = [ "10.10.10.128" ];
      relays = [ "10.10.10.128" ];
      staticHostMap = {
        "10.10.10.128" = [
          "192.168.0.128:4242"
          "122.249.92.87:4242"
        ];
      };
      firewall = {
        inbound = [
          { port = "any"; proto = "icmp"; host = "any"; }
          { port = 22; proto = "tcp"; host = "any"; }
          { port = 8384; proto = "tcp"; host = "any"; } # syncthing
        ];
        outbound = [{ port = "any"; proto = "any"; host = "any"; }];
      };
      settings.preferred_ranges = [ "192.168.0.0/24" ];
    };

    sops.secrets = {
      nebula_crt = secretsConf;
      nebula_key = secretsConf;
    };
  };
}
