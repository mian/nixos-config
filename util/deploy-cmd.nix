{ pkgs, ... }:
box: pkgs.writeShellScriptBin "${box}_deploy" ''
  set -e

  # Build
  echo "Building..."
  mkdir -p ./builds
  nix build .#nixosConfigurations.${box}.config.system.build.toplevel --out-link ./builds/${box} --log-format internal-json -v |& nom --json
  OUTPUT=$(readlink ./builds/${box})
  echo "Built: ''${OUTPUT}"

  # Sign
  sudo nix store sign -k /var/keys/nix-cache-key.priv -r ''${OUTPUT}

  # Copy
  nix copy "''${OUTPUT}" --to ssh://ian@${box}

  # Switch profiles
  SWITCH_CMD="/run/current-system/sw/bin/nix-env -p /nix/var/nix/profiles/system --set ''${OUTPUT} && /nix/var/nix/profiles/system/bin/switch-to-configuration switch"
  ssh -t ${box} "sudo sh -c \"''${SWITCH_CMD}\""
''
