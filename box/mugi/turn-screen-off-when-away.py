import glob
import paho.mqtt.client as mqtt
import subprocess

username = "readonly"
with open("/run/secrets/mqtt_readonly_pass", "r") as file:
  password = file.read().rstrip()


def get_sway_socket():
  return glob.glob("/run/user/1000/sway-ipc.*")[0]


def send_sway_cmd(cmd):
  result = subprocess.run(
    ["/etc/profiles/per-user/ian/bin/swaymsg", cmd],
    capture_output=True,
    text=True,
    env={"SWAYSOCK": get_sway_socket()},
  )
  result.check_returncode()


def on_connect(client, userdata, flags, reason, props):
  print(f"Connected ({reason})")
  client.subscribe("ha_presence")
  send_sway_cmd("output * dpms on")


def on_message(client, userdata, msg):
  print(f"Msg: {msg.topic} {str(msg.payload)}")
  if msg.payload == b"1":
    print("ON")
    send_sway_cmd("output * dpms on")
  else:
    print("OFF")
    send_sway_cmd("output * dpms off")


cli = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
cli.username_pw_set(username, password)
cli.on_connect = on_connect
cli.on_message = on_message

cli.connect("futaba")

cli.loop_forever()
