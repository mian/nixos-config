{ config, pkgs, inputs, ... }:
let
  inherit (builtins) attrNames fromJSON readFile;
  robotsConf = ''
    add_header Content-Type text/plain;
    return 200 "User-agent: *\nDisallow: /\n";
  '';
  botList = attrNames (fromJSON (readFile "${inputs.ai-robots-txt}/robots.json"));
  botUnion = pkgs.lib.concatStringsSep "|" botList;
  botBlockExtraConf = ''
    if ($http_user_agent ~* "${botUnion}") {
      return 444;
    }
  '';
in
{
  services.nginx.upstreams."backend_conduwuit".servers."makoto:6167" = { };

  services.nginx.virtualHosts."rainingmessages.dev" = {
    serverName = "rainingmessages.dev";
    root = "/var/www";
    forceSSL = true;
    enableACME = true;
    listen = [
      {
        addr = "0.0.0.0";
        port = 80;
      }
      {
        addr = "[::]";
        port = 80;
      }
      {
        addr = "0.0.0.0";
        port = 443;
        ssl = true;
      }
      {
        addr = "[::]";
        port = 443;
        ssl = true;
      }
      {
        addr = "0.0.0.0";
        port = 8448;
        ssl = true;
      }
      {
        addr = "[::]";
        port = 8448;
        ssl = true;
      }
    ];
    locations."=/robots.txt".extraConfig = robotsConf;
    locations."/_matrix/" = {
      proxyPass = "http://backend_conduwuit$request_uri";
      proxyWebsockets = true;
      extraConfig = ''
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_read_timeout 600;
        ${botBlockExtraConf}
      '';
    };
    locations."=/.well-known/matrix/server" = {
      alias = pkgs.writeText "well-known-matrix-server" ''
        {
          "m.server": "rainingmessages.dev:443"
        }
      '';
      extraConfig = ''
        # Set the header since by default NGINX thinks it's just bytes
        default_type application/json;
        ${botBlockExtraConf}
      '';
    };
    locations."=/.well-known/matrix/client" = {
      alias = pkgs.writeText "well-known-matrix-client" ''
        {
          "m.homeserver": {
            "base_url": "https://rainingmessages.dev"
          }
        }
      '';
      extraConfig = ''
        # Set the header since by default NGINX thinks it's just bytes
        default_type application/json;

        # https://matrix.org/docs/spec/client_server/r0.4.0#web-browser-clients
        add_header Access-Control-Allow-Origin "*";
        ${botBlockExtraConf}
      '';
    };
    extraConfig = ''
      merge_slashes off;
      location /.well-known/webfinger {
        rewrite ^.*$ https://social.rainingmessages.dev/.well-known/webfinger permanent;
        ${botBlockExtraConf}
      }

      location /.well-known/host-meta {
        rewrite ^.*$ https://social.rainingmessages.dev/.well-known/host-meta permanent;
        ${botBlockExtraConf}
      }

      location /.well-known/nodeinfo {
        rewrite ^.*$ https://social.rainingmessages.dev/.well-known/nodeinfo permanent;
        ${botBlockExtraConf}
      }
    '';
  };
}
