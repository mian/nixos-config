{ config, pkgs, inputs, ... }:
let
  blocky = import ../../modules/home-network-only/blocky.nix { inherit pkgs; };
  borgbackup = import ./backup.nix { inherit pkgs config; };
in
{
  services = {
    inherit blocky borgbackup;

    nebula.networks.asgard.firewall.inbound = [
      { port = 4533; proto = "tcp"; host = "any"; } # navidrome
      { port = 8096; proto = "tcp"; host = "any"; } # jellyfin
      { port = 8989; proto = "tcp"; host = "any"; } # grafana
    ];

    navidrome = {
      enable = true;
      settings = {
        Address = "0.0.0.0";
        EnableInsightsCollector = false;
        MusicFolder = "/srv/music";
        ScanSchedule = "@daily";
      };
    };

    scrutiny = {
      enable = true;
      openFirewall = true;
      collector.enable = true;
      settings.web.listen.port = 8099;
    };

    syncthing.guiAddress = "0.0.0.0:8384";

    wyoming.faster-whisper.servers.default = {
      enable = true;
      # model =
      uri = "tcp://0.0.0.0:10300";
      language = "en";
    };

    wyoming.piper.servers.default = {
      enable = true;
      voice = "en_US-libritts_r-medium";
      uri = "tcp://0.0.0.0:10200";
    };
  };

  systemd.services.pmbridge = {
    serviceConfig = {
      Type = "simple";
      User = "ian";
    };
    wantedBy = [ "multi-user.target" ];
    environment = {
      PASSWORD_STORE_DIR = "/home/ian/.local/share/password-store";
    };
    path = [
      pkgs.protonmail-bridge
      pkgs.pass
    ];
    script = "protonmail-bridge -n";
  };

  systemd.services.pueue = {
    serviceConfig = {
      Type = "simple";
      User = "ian";
    };
    wantedBy = [ "multi-user.target" ];
    path = [
      pkgs.yt-dlp
      pkgs.pueue
      pkgs.aria2
    ];
    script = "pueued -v";
  };
}
