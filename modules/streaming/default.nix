{ pkgs, config, lib, inputs, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc listOf path;
  cfg = config.my.streaming;
in
{
  options.my.streaming = {
    enable = mkOption {
      type = bool;
      default = false;
      description = mdDoc "Whether to enable my streaming environment.";
    };
  };

  config = mkIf cfg.enable (import ./streaming.nix { inherit pkgs config inputs; });
}
