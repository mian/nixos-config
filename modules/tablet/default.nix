{ pkgs, config, lib, inputs, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc listOf path;
  cfg = config.my.tablet;
in
{
  options.my.tablet = {
    enable = mkOption {
      type = bool;
      default = false;
      description = mdDoc "Whether to enable my tablet environment.";
    };
  };

  config = mkIf cfg.enable (mkMerge [
    (import ./tablet-system.nix { inherit pkgs config inputs; })
    { home-manager.users.ian = import ./tablet-home.nix { inherit pkgs config inputs; }; }
  ]);
}
