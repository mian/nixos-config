{ config, pkgs, inputs, lib, modulesPath, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ./headless-sway-vnc.nix
    ./euremote-sync.nix
  ];

  my.desktop.enable = true;
  my.home-network-only.enable = true;

  networking.firewall.allowedTCPPorts = [ 8384 ];

  # waybar pulseaudio module dies horribly without audio in 24.05
  home-manager.users.ian.programs.waybar.settings.main.modules-right = pkgs.lib.mkForce [
    "cpu"
    "memory"
    "disk"
    "network"
    "clock"
  ];

  # xdg-desktop-portal-gtk failing startup on kernel 6.10
  xdg.portal.enable = pkgs.lib.mkForce false;
  xdg.portal.extraPortals = pkgs.lib.mkForce [ ];

  home-manager.users.ian.systemd.user.services.autotiling.Service.ExecStartPre = "/run/current-system/sw/bin/sleep 5";
  home-manager.users.ian.systemd.user.services.sworkstyle.Service.ExecStartPre = "/run/current-system/sw/bin/sleep 5";

  services.kanata.enable = pkgs.lib.mkForce false;

  system.stateVersion = "24.05";
}
