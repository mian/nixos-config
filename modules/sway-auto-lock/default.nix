{ pkgs, config, lib, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc listOf path;
in
{
  options.my.sway-auto-lock = {
    enable = mkOption {
      type = bool;
      default = false;
      description = mdDoc "Whether to suspend and lock the Sway session on idle.";
    };
  };

  config = mkIf config.my.sway-auto-lock.enable (import ./sway-auto-lock.nix { inherit pkgs; });
}
