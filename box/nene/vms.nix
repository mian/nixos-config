{ config, pkgs, inputs, ... }:
let
  inherit (inputs) nixvirt;
  poolConf = {
    name = "nixvirtpool";
    uuid = "494d39dd-f4d2-451c-bb50-19e6231e1cb4";
    type = "dir";
    target.path = "/var/lib/libvirt/images/";
  };
  volumeConf = {
    name = "nixvirtdisk1.qcow2";
    uuid = "490a6d00-e062-43ed-a171-21d1fafe1f92";
    target.format.type = "qcow2";
    capacity = { count = 40; unit = "GB"; };
  };
  networkConf = {
    name = "default";
    uuid = "502e306c-cb86-4454-bce3-9cbc79bd1288";
    forward = {
      mode = "nat";
      nat = {
        port = {
          start = 1024;
          end = 65535;
        };
      };
    };
    bridge.name = "virbr0";
    ip = {
      address = "192.168.100.1";
      netmask = "255.255.255.0";
      dhcp.range = {
        start = "192.168.100.128";
        end = "192.168.100.254";
      };
    };
  };
  domainConf = (nixvirt.lib.domain.templates.linux {
    name = "disko-test-libvirt";
    # uuidgen -r
    uuid = "cc696711-ace8-45c4-bec3-8d9e3287082f";
    memory = { count = 4; unit = "GiB"; };
    vcpu = {
      placement = "auto";
      count = 4;
      current = 4;
    };
    storage_vol = {
      pool = poolConf.name;
      volume = volumeConf.name;
    };
    install_vol = "/var/lib/libvirt/nixos-minimal-24.11.714287.a45fa362d887-x86_64-linux.iso";
  }) // {
    cpu = {
      mode = "host-passthrough";
      check = "none";
      migratable = "on";
    };
  };
in
{
  programs.virt-manager.enable = true;

  # virtualisation.libvirtd.enable = true;

  virtualisation.libvirt = {
    enable = true;
    connections."qemu:///session" = {
      pools = [{
        definition = nixvirt.lib.pool.writeXML poolConf;
        active = true;
        volumes = [{
          definition = nixvirt.lib.volume.writeXML volumeConf;
        }];
      }];
      networks = [{
        definition = nixvirt.lib.network.writeXML networkConf;
        active = true;
      }];
      domains = [
        # XXX no UEFI support yet, using virt-install for now. Come back to this later
        # {
        #   definition = nixvirt.lib.domain.writeXML (nixvirt.lib.domain.templates.linux
        #     {
        #       name = "disko-test-libvirt";
        #       # uuidgen -r
        #       uuid = "cc696711-ace8-45c4-bec3-8d9e3287082f";
        #       memory = { count = 4; unit = "GiB"; };
        #       storage_vol = {
        #         pool = poolConf.name;
        #         volume = volumeConf.name;
        #       };
        #       install_vol = "/var/lib/libvirt/nixos-minimal-24.11.714287.a45fa362d887-x86_64-linux.iso";
        #     });
        # }
      ];
    };
  };
}
