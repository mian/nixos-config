{ pkgs, lib, config, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc;
in
{
  options.my.audio = {
    enable = mkOption {
      type = bool;
      default = false;
      description = mdDoc "Whether to enable my audio settings.";
    };
  };

  config = mkIf config.my.audio.enable {
    environment.systemPackages = [ pkgs.pulseaudio ];

    programs.noisetorch.enable = true;

    services.pipewire = {
      enable = true;
      pulse.enable = true;
      alsa.enable = true;
      jack.enable = true;
    };
  };
}
