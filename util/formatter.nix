{ inputs, system, ... }:
let
  pkgs = inputs.nixpkgs.legacyPackages.${system};
  treefmtEval = inputs.treefmt-nix.lib.evalModule pkgs {
    projectRootFile = "flake.nix";
    programs = {
      mdformat.enable = true;
      nixpkgs-fmt.enable = true;
      prettier.enable = true;
      ruff-format.enable = true;
      shfmt.enable = true;
      taplo.enable = true;
      yamlfmt.enable = true;
    };
    settings = {
      excludes = [
        "*.conf"
        "*.crt"
        "*.lock"
        "*.patch"
        "*.pem"
        "*.txt"
      ];
      on-unmatched = "debug";
      formatter.ruff-format.options = [ "--config" "indent-width=2" ];
    };
  };
in
treefmtEval.config.build.wrapper
