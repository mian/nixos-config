let
  passphraseFile = "/tmp/secret.key";
  swapsize = "32G";
in
{
  inherit (import ../modules/workstation-disk-encryption/disko-luks-btrfs.nix { inherit swapsize; }) disko;
  disko.devices.disk.main.content.partitions.luks.content.passwordFile = passphraseFile;
}
