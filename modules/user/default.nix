{ pkgs, lib, inputs, config, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc;
in
{
  imports = [ inputs.home-manager.nixosModules.home-manager ];

  options.my.user = {
    enable = mkOption {
      type = bool;
      default = true;
      description = mdDoc "Whether to enable my user settings.";
    };
  };

  config = mkIf config.my.user.enable (mkMerge [
    (import ./user-system.nix { inherit pkgs inputs config; })
    (import ./user-home.nix { inherit pkgs inputs config; })
  ]);
}
