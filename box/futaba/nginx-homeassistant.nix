{ ... }:
let
  vhost = serverName: {
    inherit serverName;
    enableACME = false;
    forceSSL = false;
    listen = [
      { addr = "0.0.0.0"; port = 8091; }
      { addr = "[::]"; port = 8091; }
    ];
    locations."/" = {
      proxyPass = "http://127.0.0.1:8123";
      extraConfig = ''
        proxy_set_header Host $host;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
      '';
    };
  };
in
{
  services.nginx.virtualHosts = {
    homeAssistantByIp = vhost "192.168.0.128";
    homeAssistantByDomain = vhost "futaba";
  };
}
