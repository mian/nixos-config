# .nix

Nix configuration for my personal machines. Manages workstations, servers, laptops, and a few Raspberry Pis.

## Boxes of Note

| Box                                    | Note                                                                                                                                                                                  |
| -------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [nene](./box/nene)                     | Development workstation and builder. [Mini tower](https://www.fractal-design.com/products/cases/define/define-nano-s/) sitting at my desk. Runs Sway.                                 |
| [yuuri](./box/yuuri)                   | Travel and conference notebook. [GPD Pocket 4](https://gpd.hk/gpdpocket4) running a copy of my development setup.                                                                     |
| [futaba](./box/futaba)                 | Busiest server. Headless NUC. Runs reverse proxy for home services.                                                                                                                   |
| [ranni](./box/ranni)                   | Self-built NAS. ASRock Rack board, [uNAS chassis](https://www.u-nas.com/xcart/cart.php?target=product&product_id=17640&category_id=249). Runs ZFS. Holds media, archives, backups.    |
| [mugi](./box/mugi)                     | Home control panel. Raspberry Pi 5 attached to [touchscreen](https://www.raspberrypi.com/products/touch-display-2/), sitting on my shelf. Provides Home Assistant dashboard, buttons. |
| [nano](./box/nano)                     | Headless remote desktop server. RK3588S SBC running a persistent Sway environment, accessible from anywhere.                                                                          |
| [mika](./box/mika), [rika](./box/rika) | Pi4 k3s cluster living in a [mini rack](https://deskpi.com/products/deskpi-rackmate-t1-2).                                                                                            |

## Getting Started

### Prerequisites

Requires [NixOS](https://nixos.org/download.html) with flakes enabled.

### Install

```console
$ git clone https://codeberg.org/mian/nixos-config.git ~/.nix
$ cd ~/.nix

# Add box-specific items. See others in box/*
$ vim box/somebox/default.nix

# Enter builder shell, update dependencies, build!
$ nix develop
$ nixup
$ rebuild_system
```

## Organization

| Location                              | Description                                                                                                                       |
| ------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| [/box](./box)                         | Host-specific config items and top-level entrypoints. `box/foo/default.nix` replaces the standard `/etc/nixos/configuration.nix`. |
| [/common](./common)                   | Shared configs used by many boxes and modules.                                                                                    |
| [/modules](./modules)                 | Custom NixOS modules used to mix in config sets. Exposed as `my.<option>.enable`, etc.                                            |
| [/modules/desktop](./modules/desktop) | Workstation GUI environment. Runs Sway.                                                                                           |
| [/modules/tablet](./modules/tablet)   | Touch-enabled GUI environment. Runs KDE Plasma.                                                                                   |
| [/modules/user](./modules/user)       | Common user settings, including Home Manager.                                                                                     |
| [/secrets](./secrets)                 | Secrets encrypted by sops and accessed via sops-nix.                                                                              |
| [/system](./system)                   | Old system-level configs and leftovers in the process of being cleaned up and moved to `/modules`.                                |
| [/util](./util)                       | Build and operation utilities, shells and formatters, etc. Not referenced directly by `nixosConfigurations`.                      |

## License: [Unlicense](./UNLICENSE)

This is free and unencumbered software released into the public domain.
