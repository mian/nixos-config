{ pkgs, ... }:
pkgs.writeShellScriptBin "switch-ws-and-run" ''
  swaymsg "workspace number ''${1}"
  if ! pgrep $2 > /dev/null
  then
    sleep 0.05
    swaymsg "exec ''${2}"
  fi
''
