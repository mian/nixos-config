{ pkgs, lib, inputs, config, hostName, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc listOf path;
in
{
  imports = [
    inputs.disko.nixosModules.disko
    inputs.sops-nix.nixosModules.sops
  ];

  options.my.base-system = {
    enable = mkOption {
      type = bool;
      default = true;
      description = mdDoc "Whether to enable the base system.";
    };
  };

  options.my.base-boot = {
    enable = mkOption {
      type = bool;
      default = true;
      description = mdDoc "Whether to enable the base boot config.";
    };
  };

  config = mkMerge [
    (mkIf config.my.base-system.enable (import ./base-system.nix { inherit pkgs inputs config hostName; }))
    (mkIf config.my.base-boot.enable (import ./base-boot.nix { inherit pkgs; }))
  ];
}
