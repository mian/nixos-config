{ pkgs, ... }:
let
  lockTimeSec = 600;
  suspendTimeSec = 900;
in
{
  home-manager.users.ian.services.swayidle = {
    enable = true;
    timeouts = [
      { timeout = lockTimeSec; command = "${pkgs.chayang}/bin/chayang -d 20 && ${pkgs.systemd}/bin/loginctl lock-session"; }
      { timeout = suspendTimeSec; command = "${pkgs.systemd}/bin/systemctl suspend"; }
    ];
    events = [
      { event = "before-sleep"; command = "${pkgs.systemd}/bin/loginctl lock-session"; }
      { event = "lock"; command = "${pkgs.swaylock}/bin/swaylock -f"; }
    ];
  };

  home-manager.users.ian.programs.swaylock = {
    enable = true;
    settings = {
      color = "3f3f3f";
      font-size = 24;
      image = "/home/ian/.background-image-locked";
      ignore-empty-password = true;
      indicator-idle-visible = false;
      indicator-radius = 100;
      line-color = "ffffff";
      show-failed-attempts = true;
    };
  };
}
