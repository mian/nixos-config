{ config, pkgs, inputs, ... }:
let
  inherit (builtins) attrNames fromJSON readFile;
  robotsConf = ''
    add_header Content-Type text/plain;
    return 200 "User-agent: *\nDisallow: /\n";
  '';
  botList = attrNames (fromJSON (readFile "${inputs.ai-robots-txt}/robots.json"));
  botUnion = pkgs.lib.concatStringsSep "|" botList;
  botBlockExtraConf = ''
    if ($http_user_agent ~* "${botUnion}") {
      return 444;
    }
  '';
in
{
  enable = true;
  user = "nginx";
  clientMaxBodySize = "40M";
  virtualHosts = {
    default = {
      serverName = "default";
      # root = "/var/www";
      default = true;
      locations."/" = {
        return = "444";
      };
    };
    invid = {
      enableACME = false;
      forceSSL = false;
    };
    "ian.tokyo" = {
      serverName = "ian.tokyo";
      root = "/var/www";
      forceSSL = true;
      enableACME = true;
      extraConfig = ''
        charset utf-8;
        # hacky pres alias
        rewrite ^/disko$ https://pad.rainingmessages.dev/s/NcPk1hmB7 redirect;
        ${botBlockExtraConf}
      '';
      locations."=/robots.txt".extraConfig = robotsConf;
    };
    "bin.ian.tokyo" = {
      serverName = "bin.ian.tokyo";
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://fuuka:8088";
        extraConfig = ''
          limit_except GET HEAD {
            auth_basic secured;
            auth_basic_user_file /etc/nginx/wastebin.htpasswd;
          }
          ${botBlockExtraConf}
        '';
      };
      locations."=/robots.txt".extraConfig = robotsConf;
    };
    "git.ian.tokyo" = {
      serverName = "git.ian.tokyo";
      forceSSL = true;
      enableACME = true;
      locations."/".proxyPass = "http://anzu:3000";
      locations."/".extraConfig = botBlockExtraConf;
      locations."=/robots.txt".extraConfig = robotsConf;
    };
    "todo.ian.tokyo" = {
      serverName = "todo.ian.tokyo";
      forceSSL = true;
      enableACME = true;
      locations."/".proxyPass = "http://anzu:3456";
      locations."/".extraConfig = botBlockExtraConf;
      locations."=/robots.txt".extraConfig = robotsConf;
    };
    "pad.rainingmessages.dev" = {
      serverName = "pad.rainingmessages.dev";
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        recommendedProxySettings = true;
        proxyWebsockets = true;
        proxyPass = "http://makoto:3000";
        extraConfig = botBlockExtraConf;
      };
      locations."=/robots.txt".extraConfig = robotsConf;
    };
    "social.rainingmessages.dev" = {
      serverName = "social.rainingmessages.dev";
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        recommendedProxySettings = true;
        proxyWebsockets = true;
        proxyPass = "http://fuuka:8080";
        extraConfig = botBlockExtraConf;
      };
      locations."=/robots.txt".extraConfig = robotsConf;
    };
    "vid.rainingmessages.dev" = {
      serverName = "vid.rainingmessages.dev";
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        recommendedProxySettings = true;
        proxyWebsockets = true;
        proxyPass = "http://fuuka";
        extraConfig = botBlockExtraConf;
      };
      locations."~ ^/api/v1/videos/(upload-resumable|([^/]+/source/replace-resumable))$" = {
        recommendedProxySettings = true;
        proxyWebsockets = true;
        proxyPass = "http://fuuka";
        extraConfig = ''
          client_max_body_size 0;
          proxy_request_buffering off;
          ${botBlockExtraConf}
        '';
      };
      locations."~ ^/api/v1/users/[^/]+/imports/import-resumable$" = {
        recommendedProxySettings = true;
        proxyWebsockets = true;
        proxyPass = "http://fuuka";
        extraConfig = ''
          client_max_body_size 0;
          proxy_request_buffering off;
          ${botBlockExtraConf}
        '';
      };
      locations."~ ^/api/v1/videos/(upload|([^/]+/studio/edit))$" = {
        recommendedProxySettings = true;
        proxyWebsockets = true;
        proxyPass = "http://fuuka";
        extraConfig = ''
          client_max_body_size 12G;
          add_header X-File-Maximum-Size 8G always;
          ${botBlockExtraConf}
        '';
      };
      locations."~ ^/api/v1/runners/jobs/[^/]+/(update|success)$" = {
        recommendedProxySettings = true;
        proxyWebsockets = true;
        proxyPass = "http://fuuka";
        extraConfig = ''
          client_max_body_size 12G;
          add_header X-File-Maximum-Size 8G always;
          ${botBlockExtraConf}
        '';
      };
      locations."=/robots.txt".extraConfig = robotsConf;
    };
  };
}
