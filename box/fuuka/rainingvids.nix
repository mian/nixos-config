# Simple solo video hosting instance.
# Federation, P2P, user registration all disabled.
# Runs behind shared nginx rev proxy w/TLS termination. Uses nginx locally for static assets.
{ config, pkgs, inputs, ... }:
{
  sops.secrets =
    let
      secretConf = {
        sopsFile = ../../secrets/rainingvids.yaml;
        owner = config.users.users.peertube.name;
        group = config.users.users.peertube.group;
      };
    in
    {
      rainingvids_env = secretConf;
      rainingvids_secret = secretConf;
    };

  services.nginx.virtualHosts."vid.rainingmessages.dev" = {
    serverName = "vid.rainingmessages.dev";
    enableACME = false;
    forceSSL = false;
    locations."/" = {
      extraConfig = ''
        allow 192.168.0.128;
        deny all;
      '';
    };
  };

  services.peertube = {
    enable = true;
    localDomain = "vid.rainingmessages.dev";
    listenHttp = 9999;
    listenWeb = 443;
    enableWebHttps = true;
    configureNginx = true;
    serviceEnvironmentFile = "/run/secrets/rainingvids_env";
    secrets.secretsFile = "/run/secrets/rainingvids_secret";
    database.createLocally = true;
    redis.createLocally = true;
    settings = {
      contact_form.enabled = false;
      federation.enabled = false;
      followers.instance.enabled = false;
      open_telemetry.metrics.enabled = false;
      peertube.check_latest_version.enabled = false;
      search.remote_uri.users = false;
      security.powered_by_header.enabled = false;
      signup.enabled = false;
      tracker.enabled = false;
      defaults = {
        publish = {
          comments_policy = 2;
          privacy = 2;
        };
        p2p = {
          webapp.enabled = false;
          embed.enabled = false;
        };
      };
      transcoding.resolutions = {
        "720p" = true;
        "1080p" = true;
      };
      trust_proxy = [
        "loopback"
        "192.168.0.128"
      ];
    };
  };
}
