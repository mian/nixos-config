{ config, pkgs, inputs, ... }:
let
  unstable = import ../../common/unstable.nix { inherit pkgs inputs; };
in
{
  imports = [ "${inputs.unstable}/nixos/modules/services/matrix/conduwuit.nix" ];

  sops.secrets =
    let
      secretConf = {
        sopsFile = ../../secrets/rainingpad.yaml;
        owner = config.users.users.hedgedoc.name;
        group = config.users.users.hedgedoc.group;
      };
    in
    {
      session_secret = secretConf;
      github_client_id = secretConf;
      github_client_secret = secretConf;
    };

  sops.templates."hedgedoc.conf" = {
    owner = config.users.users.hedgedoc.name;
    group = config.users.users.hedgedoc.group;
    content = ''
      CMD_SESSION_SECRET=${config.sops.placeholder.session_secret}
      CMD_GITHUB_CLIENTID=${config.sops.placeholder.github_client_id}
      CMD_GITHUB_CLIENTSECRET=${config.sops.placeholder.github_client_secret}
    '';
  };

  services = {
    conduwuit = {
      enable = true;
      package = unstable.conduwuit;
      settings = {
        global = {
          server_name = "rainingmessages.dev";
          address = [ "0.0.0.0" ];
          allow_registration = false;
          allow_federation = true;
        };
      };
    };

    hedgedoc = {
      enable = true;
      environmentFile = config.sops.templates."hedgedoc.conf".path;
      settings = {
        domain = "pad.rainingmessages.dev";
        host = "0.0.0.0";
        port = 3000;
        protocolUseSSL = true;
        email = false;
        allowAnonymous = false;
        allowAnonymousEdits = false;
        allowEmailRegister = false;
        disableNoteCreation = false; # true
        debug = false;
      };
    };

    scrutiny = {
      enable = true;
      openFirewall = true;
      collector.enable = true;
      settings.web.listen.port = 8099;
    };
  };
}
