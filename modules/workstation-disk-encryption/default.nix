{ pkgs, config, lib, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc listOf path str;
in
{
  options.my.workstation-disk-encryption = {
    enable = mkOption {
      type = bool;
      default = false;
      description = mdDoc "Whether to set up disk encryption, auto login and session locking.";
    };

    device = mkOption {
      type = str;
      default = "/dev/nvme0n1";
      description = mdDoc "Disk device to configure.";
    };

    swapsize = mkOption {
      type = str;
      default = "32G";
      description = mdDoc "Swap file size.";
    };
  };

  config = mkIf config.my.workstation-disk-encryption.enable
    (import ./workstation-disk-encryption.nix {
      inherit pkgs;
      inherit (config.my.workstation-disk-encryption) device swapsize;
    });
}
