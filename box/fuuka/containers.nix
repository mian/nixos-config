{ config, pkgs, inputs, ... }:
let
  extraOptions = [
    "--pull=missing"
  ];
  environment = {
    TZ = "Asia/Tokyo";
  };
in
{
  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
    };
    oci-containers = {
      backend = "podman";
      containers = {
        homepage = {
          inherit environment extraOptions;
          # 0.10.17
          image = "ghcr.io/gethomepage/homepage@sha256:b261c981a866a0e287205394bf365bd8cdb9152469a85ec569d7bfcd7812cf14";
          ports = [ "7575:3000" ];
          volumes = [
            "/srv/homepage:/app/config"
          ];
        };

        reddit-top-rss = {
          inherit environment extraOptions;
          # 2023.05.11
          image = "johnny5w/reddit-top-rss@sha256:f898b5b2643cdfa9bd741a8255e185a05b9ba66f969448c6672884aa187c9cb0";
          ports = [ "8089:8080" ];
          dependsOn = [ ];
          environmentFiles = [ /etc/reddit-top-rss/env ];
        };
      };
    };
  };
}
