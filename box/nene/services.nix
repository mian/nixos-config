{ pkgs, inputs, config, ... }:
let
  # unstable = import ../../common/unstable.nix { inherit pkgs inputs; };
in
{
  nix = {
    settings.secret-key-files = [
      "/var/keys/nix-cache-key.priv"
    ];
    sshServe = {
      enable = false;
      protocol = "ssh-ng";
      keys = [ ];
    };
  };

  services = {
    flatpak.enable = true;

    rabbitmq = {
      enable = false;
      listenAddress = "0.0.0.0";
      configItems = {
        "loopback_users.guest" = "false";
      };
    };

    redis.servers.dev = {
      enable = false;
      port = 6379;
      bind = null;
      settings = {
        protected-mode = "no";
      };
    };

    scrutiny = {
      enable = true;
      openFirewall = true;
      collector.enable = true;
      settings.web.listen.port = 8099;
    };
  };
}
