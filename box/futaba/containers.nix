{ config, pkgs, inputs, ... }:
{
  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
  };
  virtualisation.oci-containers.backend = "podman";

  virtualisation.oci-containers.containers = {
    homeassistant = {
      # 2025.1.2
      image = "ghcr.io/home-assistant/home-assistant@sha256:871f84a00db8d05856a70ee3761b138a8e91eb108d61f2fa176e7eeadb5eda03";
      ports = [ "127.0.0.1:8123:8123" ];
      volumes = [
        "/srv/home-assistant:/config"
        "/run/dbus:/run/dbus:ro"
      ];
      # pull = "missing"; # not in 24.11
      # capabilities.CAP_NET_RAW = true; # not in 24.11
      environment.TZ = "Asia/Tokyo";
      extraOptions = [ "--pull=missing" "--cap-add=CAP_NET_RAW" ];
    };
  };
}
