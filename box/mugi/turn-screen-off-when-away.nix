{ config, pkgs, ... }:
let
  pyEnv = pkgs.python3.withPackages (ps: [ ps.paho-mqtt_2 ]);
  pyScript = pkgs.writeShellScriptBin "turn-screen-off-when-away" ''
    ${pyEnv}/bin/python -u ${./turn-screen-off-when-away.py}
  '';
in
{
  sops.secrets.mqtt_readonly_pass.sopsFile = ../../secrets/mqtt.yaml;

  systemd.services.turn-screen-off-when-away = {
    serviceConfig = {
      Type = "simple";
      User = "root";
      ExecStartPre = [ "/bin/sh -c 'until ${pkgs.bind.host}/bin/host ian.tokyo; do sleep 10; done'" ];
    };
    requires = [ "network-online.target" ];
    after = [ "network-online.target" ];
    wantedBy = [ "network-online.target" ];
    script = "${pyScript}/bin/turn-screen-off-when-away";
  };
}
