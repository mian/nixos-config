{
  description = "NixOS configuration for my personal machines.";
  inputs = {
    nixpkgs = { url = "github:NixOS/nixpkgs/nixos-24.11"; };
    unstable = { url = "github:NixOS/nixpkgs/nixos-unstable"; };
    nixos-hardware = { url = "github:NixOS/nixos-hardware/master"; };

    ai-robots-txt = {
      url = "github:ai-robots-txt/ai.robots.txt";
      flake = false;
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    emacspkg = {
      url = "git+https://codeberg.org/mian/emacs-config";
      # url = "git+file:/home/ian/.emacs.d?shallow=1";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-artwork = {
      url = "github:NixOS/nixos-artwork";
      flake = false;
    };
    nixvirt = {
      url = "github:AshleyYakeley/NixVirt";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    plasma-manager = {
      url = "github:nix-community/plasma-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };
    raspberry-pi-nix = {
      url = "github:nix-community/raspberry-pi-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = inputs:
    let
      inherit (builtins) attrNames map mapAttrs readDir;
      forSystems = inputs.nixpkgs.lib.genAttrs [ "aarch64-linux" "x86_64-linux" ];
      baseModules = map (m: ./modules/${m}) (attrNames (readDir ./modules));
      boxModules = mapAttrs (b: _: ./box/${b}) (readDir ./box);
      boxConfig = hostName: boxModule: inputs.nixpkgs.lib.nixosSystem {
        specialArgs = { inherit inputs hostName; };
        modules = baseModules ++ [ boxModule ];
      };
    in
    {
      nixosConfigurations = mapAttrs boxConfig boxModules;
      devShells = forSystems (system: {
        default = import ./util/builder-shell.nix { inherit inputs system; };
      });
      formatter = forSystems (system: (import ./util/formatter.nix { inherit inputs system; }));
    };
}
