{ pkgs, inputs, ... }:
let
  invidOverlay = (self: super: {
    invidious = super.callPackage "${inputs.nixpkgs}/pkgs/by-name/in/invidious/package.nix" {
      crystal = super.crystal // {
        buildCrystalPackage = args:
          super.crystal.buildCrystalPackage (args // {
            version = "custom-mian";
            patches = [ ./invidious-customization.patch ];
          });
      };
    };
  });
  ptOverlay = (self: super: {
    peertube = super.peertube.overrideAttrs (prev:
      let
        version = "v6.3.3-customized";
      in
      {
        inherit version;
        src = pkgs.fetchFromGitea {
          domain = "codeberg.org";
          owner = "mian";
          repo = "rainingvids";
          rev = version;
          hash = "sha256-ck6E5/101yP1VVYBI7XwrREbVS1Uwy1WItNeR2oSE/0=";
        };
      });
  });
in
[
  invidOverlay
  ptOverlay
]
