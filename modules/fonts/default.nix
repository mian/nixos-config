{ pkgs, lib, config, ... }:
let
  inherit (lib) mkIf mkMerge mkOption;
  inherit (lib.types) bool mdDoc listOf path;
in
{
  options.my.fonts = {
    enable = mkOption {
      type = bool;
      default = false;
      description = mdDoc "Whether to enable my font settings.";
    };
  };

  config = mkIf config.my.fonts.enable {
    fonts = import ./fonts.nix { inherit pkgs; };
  };
}
